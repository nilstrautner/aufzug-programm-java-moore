import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.shape.*;  
import javafx.scene.image.Image;     
import javafx.scene.image.*;                           
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;      
import java.io.*;
import java.util.*;

public class Aufzug_Programm extends Application implements EventHandler {
  
  //JavaFX Elemente
  Button elevator;
  Button rfs[];
  Text label;  
  
  //Init
  int[] OGy = {414, 207, 12}; //Limits
  boolean k0, k1, k2, s0, s1, s2;
  int zustand = 0;
  boolean warten = false;
  int wartezeit = 1000; //Wartezeit bei Zwischenstoppen in Millisekunden
  
  Aufzug_Motor motorthread = new Aufzug_Motor(this, OGy[0]);     //Aktuelle Position des Aufzugs nur in motorthread.posY
  Aufzug_Logic logicthread = new Aufzug_Logic(this);             //Handler 
  
  public void start(Stage window) throws Exception {
    //Objs
    Group group = new Group();
    Image im = new Image("file:images/bg.jpg");
    ImageView mv = new ImageView(im);
    group.getChildren().add(mv);
    
    //Elevator Aufzug
    elevator = new Button("");
    Image speichern = new Image(getClass().getResourceAsStream("images/elevator.jpg"), 106, 157, false, false);
    elevator.setGraphic(new ImageView(speichern)); 
    elevator.setStyle("-fx-background-color: transparent;");  
    elevator.setPrefSize(106, 157);
    elevator.setLayoutX(300);
    elevator.setLayoutY(OGy[0]);
    group.getChildren().add(elevator);
    
    Rectangle labelbg = new Rectangle(8, 8, 35, 28);
    labelbg.setFill(Color.TRANSPARENT);
    labelbg.setStroke(Color.BLACK);
    group.getChildren().add(labelbg);  
    
    label = new Text("E0");
    label.setLayoutX(10);
    label.setLayoutY(30);
    label.setStyle("-fx-font: 24 arial;");
    group.getChildren().add(label);
    
    //Threads
    Thread th = new Thread(motorthread);
    th.start();
    Thread th2 = new Thread(logicthread);
    th2.start();
    
    //Rufsignale rfs als Array
    rfs = new Button[3];
    for (int i = 2; i>=0;i-- ) {
      rfs[i] = new Button();
      rfs[i].setPrefSize(20, 20);
      rfs[i].setLayoutX(280);
      rfs[i].setLayoutY(OGy[i] + 157/2-10);
      rfs[i].setStyle("-fx-background-color: rgba(0,0,0,0.08), linear-gradient(#9a9a9a, #909090), linear-gradient(white 0%, #f3f3f3 50%, #ececec 51%, #f2f2f2 100%); -fx-background-insets: 0 0 -1 0,0,1; -fx-background-radius: 5,5,4;");
      rfs[i].setOnAction(this);
      group.getChildren().add(rfs[i]);
    } // end of for
    
    //Stage Einstellungen
    
    Scene scene = new Scene(group, 440, 600);
    window.setScene(scene);
    window.setResizable(false);
    window.setTitle("Aufzug Programm");
    window.show();
    
    window.setScene(scene);
    window.show();
  }

  public void handle(Event arg0) {
    //2. OG (oberster Knopf)
    if (arg0.getSource() == rfs[2]) {
      k2 = true;
    }
    //1. OG (mittlerer Knopf)
    if (arg0.getSource() == rfs[1]) {
      k1 = true;
    }
    //0. OG (unterer Knopf)
    if (arg0.getSource() == rfs[0]) {
      k0 = true;
    }
  }
  
  public void ermittleFolgeZustand() {
    if (warten) return;
    int alterzustand = zustand;
    switch (zustand) {
      case  0: //Steht EG (0)
        if(!k1 && !k2) zustand = 0;
        if(k1 && !k2) zustand = 3;
        if(!k1 && k2) zustand = 4;
        if(k1 && k2) zustand = 3; 
        motorthread.driveUp = false; 
        motorthread.driveDown = false;  
        label.setText("E0"); 
        k0 = false;  
        s0 = true; s1 = false; s2 = false;
        break;
      case  1: //Steht im 1. OG (1)
        if(!k0 && !k2) zustand = 1;
        if(!k0 && k2) zustand = 4;
        if(k0) zustand = 7;              
        motorthread.driveUp = false; 
        motorthread.driveDown = false;  
        label.setText("O1"); 
        k1 = false;
        s0 = false; s1 = true; s2 = false;
        break;
      case 2: //Steht im 2. OG (2)
        if(!k0 && !k1) zustand = 2;
        if(!k0 && k1) zustand = 6;
        if(k0 && !k1) zustand = 7;
        if(k0 && k1) zustand = 6;             
        motorthread.driveUp = false; 
        motorthread.driveDown = false;  
        label.setText("O2"); 
        k2 = false;
        s0 = false; s1 = false; s2 = true;
        break;
      case 3: //F�hrt hoch ins 1. OG (0-1)
        if(!s1) zustand = 3;
        if(!k2 && s1) zustand = 1;
        if(k2 && s1) zustand = 5;
        motorthread.driveUp = true; 
        motorthread.driveDown = false;  
        break;
      case 4: //F�hrt hoch ins 2. OGy (1-2)
        if(!s2) zustand = 4;
        if(s2) zustand = 2;
        motorthread.driveUp = true; 
        motorthread.driveDown = false;  
        break;
      case 5: //Stop im 1. OGy (hoch)
        if(!s1 && !s2) zustand = 5;
        if(s1 && !s2) zustand = 4;
        motorthread.driveUp = false; 
        motorthread.driveDown = false;  
        k1 = false;
        s0 = false; s1 = true; s2 = false;
        break;
      case 6: //F�hrt runter ins 1. OG (2-1)
        if(!s1) zustand = 6;
        if(!k0 && s1) zustand = 1;
        if(k0 && s1) zustand = 8;
        motorthread.driveUp = false; 
        motorthread.driveDown = true;  
        break;
      case 7: //F�rt runter ins EG (1-0)
        if(!s0) zustand = 7;
        if(s0) zustand = 0;
        motorthread.driveUp = false; 
        motorthread.driveDown = true;  
        break;
      case 8:   //Stop im 1. OG (runter)
        if(!s1) zustand = 8;
        if(k0 && s1) zustand = 7;
        motorthread.driveUp = false; 
        motorthread.driveDown = false;  
        k1 = false;
        s0 = false; s1 = true; s2 = false;
        break;
      default: 
    } // end of switch
    if (alterzustand != zustand) System.out.println("Zustand: " + zustand);  
  }
  
  public void checkAngekommen() {
    try {
      switch (zustand) {
        case 3: //F�hrt ins 1. OG (0-1)    
          if (this.elevator.getLayoutY() < OGy[1]) {
            s1 = true;
          }
          break;
        case 4:
          if (this.elevator.getLayoutY() < OGy[2] ) {
            s2 = true;
          }
          break;
        case 5:
          try {
            label.setText("O1"); 
            motorthread.driveUp = false; 
            motorthread.driveDown = false;  
            Thread.sleep(wartezeit);
            motorthread.driveUp = true; 
            motorthread.driveDown = false; 
          } catch(Exception e) {
            e.printStackTrace();
          } 
          break;
        case 6:
          if (this.elevator.getLayoutY() > OGy[1] ) {
            s1 = true;
          }
          break;                      
        case 7:
          if (this.elevator.getLayoutY() > OGy[0]) {
            s0 = true;
          }
          break;
        case 8:
          try {
            label.setText("O1"); 
            motorthread.driveUp = false; 
            motorthread.driveDown = false;  
            Thread.sleep(wartezeit);
            motorthread.driveUp = false; 
            motorthread.driveDown = true; 
          } catch(Exception e) {
            e.printStackTrace();
          } 
          break;
        default: 
      } // end of switch
    } catch(Exception e) {
      e.printStackTrace();
    } 
  }
    
  public static void main(String[] args) {
    launch(args);
  } // end of main
    
} // end of class Aufzug_Programm
    
