# Aufzug Programm Java Moore

Ein Aufzug Programm mit Java, basierend auf logischer Basis des Moore-Automats und grafischer Oberfläche mit Animationen mittels JavaFX.

![Aufzug Demo](images/md/preview.gif)

> Dieses Projekt war Teil der Fachrichtung Technik, Schwerpunkt Datenverarbeitungstechnik der gymnasialen Oberstufe und wurde mit 15 Punkten (Note 1) bewertet.
